import { NgModule } from "@angular/core";
import { AccountPageComponent } from "./account.component";
import { CommonModule } from "@angular/common";
import {RouterModule} from "@angular/router"

@NgModule({
    declarations: [
        AccountPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
               path: '',
               component: AccountPageComponent 
            }
        ])
    ]
})

export class AccountPageModule {}