import { NgModule } from "@angular/core";
import { AuthPageComponent } from "./auth.component";
import { CommonModule } from "@angular/common";
import {RouterModule} from "@angular/router"

@NgModule({
    declarations: [
        AuthPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
               path: '',
               component: AuthPageComponent 
            }
        ])
    ]
})

export class AuthPageModule {}